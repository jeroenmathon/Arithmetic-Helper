//
// Created by jeroen on 7/5/15.
//

#include "arithmetic.h"

Arithmetic::Arithmetic()
{

}

Arithmetic::~Arithmetic()
{

}

void Arithmetic::setLength(int input)
{
    length = input;
}

void Arithmetic::setDifficulty(int input)
{
    difficulty = input;
}

int Arithmetic::setMode(int input)
{
    if(input > 4 || input < 0) return -1;
    else
    {
        mode = input;
    }

    return 0;
}

void Arithmetic::printScore()
{
    std::cout << "Your score is:" << correct << "/" << length << "\n";
}

void Arithmetic::resetNums()
{
    leftNum = 0;
    rightNum = 0;
}

unsigned long int Arithmetic::genNumber(int difficulty)
{
    /* Get random seed */
    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    srand(ts.tv_nsec);

    std::stringstream temp;
    unsigned long int output(0);

    switch(difficulty)
    {
        case 1:
            for(auto i(0); i < 2; i++) temp << rand() % (9 - 1) + 1;
            break;

        case 2:
            for(auto i(0); i < 3; i++) temp << rand() % (9 - 1) + 1;
            break;

        case 3:
            for(auto i(0); i < 5; i++) temp << rand() % (9 - 1) + 1;
            break;

        case 4:
            for(auto i(0); i < 10; i++) temp << rand() % (9 - 1) + 1;
            break;

        default:
            break;
    }

    temp >> output;
    return output;

}

void Arithmetic::practice()
{
    /* Reset score */
    incorrect= 0; correct = 0;

    switch(mode)
    {
        case 1: // Additions
            for(auto i(0); i < length; i++)
            {
                // Get input and numbers
                resetNums();

                std::string input;
                unsigned long int answer(0);

                while(leftNum < rightNum || leftNum == 0 || rightNum == 0)
                {
                    leftNum = genNumber(difficulty);
                    rightNum = genNumber(difficulty);
                }
                std::cout << leftNum << "+" << rightNum << "=";
                std::cin >> input;

                stringToUnsignedLongInt(input, answer);

                if(answer == leftNum + rightNum) correct++;
                else incorrect++;
            }

            printScore();
            break;

        case 2: // Subtractions
            for(auto i(0); i < length; i++)
            {
                // Get input and numbers
                resetNums();

                std::string input;
                unsigned long int answer(0);

                while(leftNum < rightNum || leftNum == 0 || rightNum ==0)
                {
                    leftNum = genNumber(difficulty);
                    rightNum = genNumber(difficulty);
                }

                std::cout << leftNum << "-" << rightNum << "=";
                std::cin >> input;

                stringToUnsignedLongInt(input, answer);

                if(answer == leftNum - rightNum) correct++;
                else incorrect++;
            }

            printScore();
            break;

        case 3: // Multiplications
            for(auto i(0); i < length; i++)
            {
                // Get input and numbers
                resetNums();

                std::string input;
                unsigned  long answer(0);

                leftNum = genNumber(difficulty);
                rightNum = genNumber(difficulty);

                std::cout << leftNum << "*" << rightNum << "=";
                std::cin >> input;

                stringToUnsignedLongInt(input, answer);

                if(answer == leftNum * rightNum) correct++;
                else incorrect++;
            }

            printScore();
            break;

        case 4: // Divisions
            for(auto i(0); i < length; i++)
            {
                // Get input and numbers
                resetNums();
                leftNumf = 0; rightNumf = 0;

                std::string input;
                float answer(0.0f);

                do
                {
                    leftNumf = genNumber(difficulty);
                    rightNumf = genNumber(difficulty);
                }while(leftNumf < rightNumf || leftNumf == 0 || rightNumf == 0 || leftNumf / rightNumf < 1.0f
                       || (leftNumf / rightNumf) != floor(leftNumf / rightNumf));

                std::cout << leftNumf << "\\" << rightNumf << "=";
                std::cin >> input;

                stringToFloat(input, answer);

                if(answer == leftNumf / rightNumf) correct++;
                else incorrect++;
            }

            printScore();
            break;

        default:
            break;
    }
}