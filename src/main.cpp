#include <iostream>
#include <algorithm>
#include "utils.h"
#include "arithmetic.h"

int main(int argc, char **argv)
{
    Arithmetic arithmetic;

    bool exitFlag = false;

    while (!exitFlag)
    {
        std::string input;
        int option;

        std::cout << "Welcome to Arithmetic Helper\n"
        << " What would you like to practice today?\n"
        << " 1.Additions.\n"
        << " 2.Subtracktions.\n"
        << " 3.Multiplications.\n"
        << " 4.Divisions.\n"
        << " 5.Exit.\n"
        << "\nOption:";

        std::cin >> input;
        if (stringToInt(input, option) == 1)
        {
            if (option > 0 && option < 5)
            {
                if (arithmetic.setMode(option) == 0)
                {
                    bool enteredLength = false;
                    bool selectedDifficulty = false;
                    std::string input;
                    int option;

                    arithmetic.setLength(0);
                    arithmetic.setDifficulty(0);

                    while(!selectedDifficulty)
                    {
                        std::cout << "Please select the difficulty.\n"
                                    << " 1.Easy(short numbers).\n"
                                    << " 2.Normal(medium numbers).\n"
                                    << " 3.Hard(long numbers).\n"
                                    << " 4.Extreme(challanging numbers).\n"
                                    << "Option:";

                        std::cin >> input;

                        if(stringToInt(input, option) == 1)
                        {
                            if(option >= 1 && option <= 4)
                            {
                                arithmetic.setDifficulty(option);
                                selectedDifficulty = true;
                            }
                            else std::cout << "Please pick a valid difficulty!\n\n";
                        }
                        else std::cout << "Invalid input!\n\n";
                    }

                    option = 0;

                    while(!enteredLength)
                    {
                        std::cout << "How many questions would you like to have?:";
                        std::cin >> input;
                        if (stringToInt(input, option) == 1)
                        {
                            arithmetic.setLength(option);
                            enteredLength = true;
                        }
                        else std::cout << "Invalid input!\n\n";
                    }

                    arithmetic.practice(); // Start the practice
                }
            }
            else if (option == 5) exitFlag = true;
            else std::cout << "Please pick a valid option!\n\n";
        }
        else std::cout << "Invalid input!\n\n";
    }

	return 0;
}