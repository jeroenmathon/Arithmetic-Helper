//
// Created by jeroen on 7/5/15.
//

#ifndef ARITHMETIC_HELPER_UTILS_H
#define ARITHMETIC_HELPER_UTILS_H

#include <iostream>
#include <algorithm>
#include <sstream>

int stringToInt(std::string input, int &output);
int stringToUnsignedLongInt(std::string input, unsigned long int &output);
int stringToFloat(std::string input, float &output);

#endif //ARITHMETIC_HELPER_UTILS_H
