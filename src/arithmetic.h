//
// Created by jeroen on 7/5/15.
//

#ifndef ARITHMETIC_HELPER_ARITHMETIC_H
#define ARITHMETIC_HELPER_ARITHMETIC_H

#include <iostream>
#include <cstdlib>
#include <sys/time.h>
#include <sstream>
#include "utils.h"

class Arithmetic
{
public:
    // Constructors and Destructors
    Arithmetic();
    ~Arithmetic();

    // Public routines
    int setMode(int input);          // Sets the mode of arithmetic to be practiced
    void practice();                // Start the practice of the selected arithmetic mode
    void setLength(int input);      // Sets the length of the questions
    void setDifficulty(int input);  // Sets the difficulty of the questions

private:
    // Private routines
    unsigned long int genNumber(int difficulty);    // Generate a number based on the difficulty
    void printScore();                              // Prints the current score
    void resetNums();                               // Clears the left and right numbers

    // Private variables
    int mode;                               // The arithmetic that is to be practiced
    int length;                             // The amount of questions that are to be asked
    int difficulty;                         // The difficulty selected to practice on
    int incorrect, correct;                 // The score variables
    unsigned long int leftNum, rightNum;    // The numbers used in the questions
    float leftNumf, rightNumf;              // The numbers used in division questions

};

#endif //ARITHMETIC_HELPER_ARITHMETIC_H
