//
// Created by jeroen on 7/5/15.
//

#include "utils.h"
//TODO Optimize this using the template library

int stringToInt(std::string input, int &output)
{
    if(std::all_of(input.begin(), input.end(), ::isdigit))
    {
        std::stringstream temp;
        temp << input;
        temp >> output;

        return 1;
    }

    return 0;
}

int stringToUnsignedLongInt(std::string input, unsigned long int &output)
{
    if(std::all_of(input.begin(), input.end(), ::isdigit))
    {
        std::stringstream temp;
        temp << input;
        temp >> output;

        return 1;
    }

    return 0;
}

int stringToFloat(std::string input, float &output)
{
    if(std::all_of(input.begin(), input.end(), ::isdigit))
    {
        std::stringstream temp;
        temp << input;
        temp >> output;

        return 1;
    }

    return 0;
}