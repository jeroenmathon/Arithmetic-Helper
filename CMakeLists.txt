cmake_minimum_required(VERSION 3.0)
project(Arithmetic-Helper)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES src/main.cpp src/arithmetic.cpp src/utils.cpp)
add_executable(Arithmetic-Helper ${SOURCE_FILES})